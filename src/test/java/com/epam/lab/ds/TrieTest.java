package com.epam.lab.ds;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class TrieTest {

    Dictionary sut = new Trie();

    @Test
    void whenAWordIsAddedSizeIsIncremented() {
        //WHEN
        sut.add("hello");
        //THEN
        assertEquals(1, sut.size());
        //WHEN
        sut.add("world");
        //THEN
        assertEquals(2, sut.size());
    }

    @Test
    void whenAWordIsAddedItCanBeCountedByItsPrefix() {
        //WHEN
        sut.add("hello");
        //THEN
        assertEquals(1, sut.countByPrefix("hell"));
    }

    @Test
    void whenSeveralWorldsAreAddedAndOneOfThemIsASubstringItTheyCanBeCounted() {
        //WHEN
        sut.add("hello");
        sut.add("hell");
        //THEN
        assertEquals(2, sut.countByPrefix("hell"));
    }

    @Test
    void findByPrefix() {
        //WHEN
        sut.add("seven");
        sut.add("hello");
        sut.add("hell");
        sut.add("test");
        //WHEN
        List<String> all = sut.startsWith("hell");
        //THEN
        assertEquals(2, all.size());
        assertEquals(all.get(0), "hell");
        assertEquals(all.get(1), "hello");
    }

    @Test
    void findByPrefixInverted() {
        //WHEN
        sut.add("hell");
        sut.add("hello");
        //WHEN
        List<String> all = sut.startsWith("hell");
        //THEN
        assertEquals(2, all.size());
        assertEquals(all.get(0), "hell");
        assertEquals(all.get(1), "hello");
    }

}