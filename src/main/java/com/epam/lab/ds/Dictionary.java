package com.epam.lab.ds;

import java.util.List;

public interface Dictionary {

    void add(String word);

    List<String> startsWith(String prefix);

    int countByPrefix(String prefix);

    int size();

}
