package com.epam.lab.ds;

/**
 * Created by Руслан on 15.12.2017.
 */
import java.util.*;

public class App extends MyHashMap {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        MyHashMap map = new MyHashMap();
        while (true) {
            String operation = getOperation();
            switch (operation) {
                case "put": {
                    String name = getName();
                    String phone = getPhone();
                    map.put(name, phone);
                    break;
                }
                case "get": {
                    String name = getName();
                    Integer number = (Integer) map.get(name);
                    if (number == null) {
                        System.out.println(name + " not in phonebook");
                    } else {
                        System.out.println(number);
                    }
                    break;
                }
                default:
                    return;
            }
        }
    }

    private static String getOperation() {
        System.out.println("Введите выражение");
        return scanner.nextLine();
    }

    private static String getName() {
        System.out.println("Введите name");
        return scanner.nextLine();
    }

    private static String getPhone() {
        System.out.println("Введите phone");
        return scanner.nextLine();
    }

}

