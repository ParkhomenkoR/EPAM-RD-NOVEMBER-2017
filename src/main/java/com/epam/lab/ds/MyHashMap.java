package com.epam.lab.ds;

/**
 * Created by Руслан on 15.12.2017.
 */
import java.util.Iterator;

public class MyHashMap<K, V> implements Map<K, V> {
    public MyHashMap(int initialSize){
        this.BUCKET_ARRAY_SIZE = initialSize;
    }
    MyHashMap() {

    }

    private int BUCKET_ARRAY_SIZE;
    private Entry bucketArray[] = new Entry[BUCKET_ARRAY_SIZE];

    @Override
    public int size() {
        return bucketArray.length;
    }

    @Override
    public Object get(Object key) {
        int hash = Math.abs(key.hashCode() % BUCKET_ARRAY_SIZE);
        while(bucketArray[hash] != null){
            if(bucketArray[hash].getKey().equals(key)){
                return bucketArray[hash].getValue();
            }
            bucketArray[hash] = (Entry) bucketArray[hash].getValue();
        }
        return null;
    }

    @Override
    public void put(Object key, Object value) {
        int hash = Math.abs(key.hashCode() % BUCKET_ARRAY_SIZE);
        Entry n = new Entry<>(key, value);
        bucketArray[hash] = n;
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        SOList soList = new SOList();
        return (Iterator<Entry<K, V>>) soList;
    }
}

