package list;

import java.util.Arrays;

public class MyArrayList<E>{
    private static final int DEFAULT_INITIAL_CAPACITY = 5;
    private static final Object[] EMPTY_ELEMENT_DATA = {};
    private int size;

    private transient Object[] customArrayListElementData;

    public MyArrayList(int initialCapacity){

        super();
        if (initialCapacity < 0)
        throw new IllegalArgumentException("Illegal Capacity: "+
                initialCapacity);
        this.customArrayListElementData = new Object[initialCapacity];
    }

    MyArrayList(){
        super();
        this.customArrayListElementData = EMPTY_ELEMENT_DATA;
    }

    int size() {
        return size;
    }

    public boolean isEmpty() {
        return size==0;
    }

    private boolean add(E e) {
        ensureCapacity(size + 1);
        customArrayListElementData[size++] = e;
        return true;
    }

    public void clear() {
        for (int i = 0; i < size; i++)
        customArrayListElementData[i] = null;
        size = 0;
    }

    @SuppressWarnings("unchecked")
    public E get(int index) {
        if (index >= size){
            throw new ArrayIndexOutOfBoundsException("array index out of bound exception with index at"+index);
        }
        return (E)customArrayListElementData[index];
    }

    public void add(int index, E element) {
        ensureCapacity(size + 1);
        System.arraycopy(customArrayListElementData, index, customArrayListElementData, index + 1,size - index);
        customArrayListElementData[index] = element;
        size++;
    }

    @SuppressWarnings("unchecked")
    E remove() {

        E oldValue = (E)customArrayListElementData[1];
        int removeNumber = size - 1 - 1;

        if (removeNumber > 0){
            System.arraycopy(customArrayListElementData, 1 +1, customArrayListElementData, 1,removeNumber);
        }
        customArrayListElementData[--size] = null;
        return oldValue;
    }

    private void growCustomArrayList(int minCapacity) {
        int oldCapacity = customArrayListElementData.length;
        int newCapacity = oldCapacity + (oldCapacity /2);
        if (newCapacity - minCapacity < 0)
        newCapacity = minCapacity;
        customArrayListElementData = Arrays.copyOf(customArrayListElementData, newCapacity);
    }

    private void ensureCapacity(int minCapacity) {
        if (customArrayListElementData == EMPTY_ELEMENT_DATA) {
            minCapacity = Math.max(DEFAULT_INITIAL_CAPACITY, minCapacity);
        }
        if (minCapacity - customArrayListElementData.length > 0)
        growCustomArrayList(minCapacity);
    }

    public static void main(String[] args) {
        MyArrayList<String> strList= new MyArrayList<>();
        strList.add("str1");
        strList.add("str2");
        System.out.println("after adding elements size ="+strList.size());
        strList.remove();
        System.out.println("after removing element size ="+strList.size());
    }
}
