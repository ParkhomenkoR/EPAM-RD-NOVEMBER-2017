package list;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

public class MyArrayListTest extends TestCase {

    private MyArrayList<String> list;

    @Before
    public void setUp(){
        list = new MyArrayList<String>();
    }

    @Test
    public void testListInit(){
        assertTrue(list.isEmpty());
        assertTrue(list.size() == 0);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testInvalidCapacity(){
        list = new MyArrayList<String>(-1);
    }

    @Test
    public void testAddElements(){
        list.add(0, "Karol");
        list.add(1, "Vanessa");
        list.add(2, "Amanda");

        assertEquals("Karol", list.get(0));
        assertEquals("Vanessa", list.get(1));
        assertEquals("Amanda", list.get(2));

        list.add(1, "Mariana");

        assertEquals("Karol", list.get(0));
        assertEquals("Mariana", list.get(1));
        assertEquals("Vanessa", list.get(2));
        assertEquals("Amanda", list.get(3));

        assertTrue(list.size()==4);
    }

    @Test(expected = NullPointerException.class)
    public void testAddElementNull(){
        list.add(0, null);
    }

    @Test (expected = NullPointerException.class)
    public void testSetElementNull(){
        list.add(0, "Kheyla");
        list.clear();
    }

    @Test
    public void testSetElement(){
        list.add(0, "Karol");
        list.add(1, "Vanessa");
        list.add(2, "Amanda");

        list.add(1, "Livia");

        assertEquals("Karol", list.get(0));
        assertEquals("Livia", list.get(1));
        assertEquals("Amanda", list.get(2));
    }

    @Test
    public void testRemoveElement(){
        list.add(0, "Karol");
        list.add(1, "Vanessa");
        list.add(2, "Amanda");

        assertEquals("Amanda", list.remove());
        assertTrue(list.size() == 2);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testRemoveWithEmptyList(){
        list.remove();
    }
}