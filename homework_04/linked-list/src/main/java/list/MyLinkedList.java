package list;

public class MyLinkedList {
    private static int counter;
    private Node head;

    public MyLinkedList() {

    }

    public void add(Object data) {
        if (head == null) {
            head = new Node(data);
        }
        Node myTemp = new Node(data);
        Node myCurrent = head;
        if (myCurrent != null) {
            while (myCurrent.getNext() != null) {
                myCurrent = myCurrent.getNext();
            }
            myCurrent.setNext(myTemp);
        }
        incrementCounter();
    }

    private static int getCounter() {
        return counter;
    }

    private static void incrementCounter() {
        counter++;
    }

    private void decrementCounter() {
        counter--;
    }

    public void add(Object data, int index) {
        Node myTemp = new Node(data);
        Node myCurrent = head;

        if (myCurrent != null) {
            for (int i = 0; i < index && myCurrent.getNext() != null; i++) {
                myCurrent = myCurrent.getNext();
            }
        }
        myTemp.setNext(myCurrent.getNext());
        myCurrent.setNext(myTemp);
        incrementCounter();
    }

    public Object get(int index)
    {
        if (index < 0)
            return null;
        Node myCurrent;
        if (head != null) {
            myCurrent = head.getNext();
            for (int i = 0; i < index; i++) {
                if (myCurrent.getNext() == null)
                    return null;
                myCurrent = myCurrent.getNext();
            }
            return myCurrent.getData();
        }
        return null;
    }

    public boolean remove(int index) {
        if (index < 1 || index > size())
            return false;
        Node myCurrent = head;
        if (head != null) {
            for (int i = 0; i < index; i++) {
                if (myCurrent.getNext() == null)
                    return false;
                myCurrent = myCurrent.getNext();
            }
            myCurrent.setNext(myCurrent.getNext().getNext());
            decrementCounter();
            return true;
        }
        return false;
    }

    public int size() {
        return getCounter();
    }

    public String toString() {
        StringBuilder output = new StringBuilder();

        if (head != null) {
            Node myCurrent = head.getNext();
            while (myCurrent != null) {
                output.append("[").append(myCurrent.getData().toString()).append("]");
                myCurrent = myCurrent.getNext();
            }
        }
        return output.toString();
    }

    private class Node {
        Node next;
        Object data;

        Node(Object dataValue) {
            next = null;
            data = dataValue;
        }

        @SuppressWarnings("unused")
        public Node(Object dataValue, Node nextValue) {
            next = nextValue;
            data = dataValue;
        }

        Object getData() {
            return data;
        }

        @SuppressWarnings("unused")
        public void setData(Object dataValue) {
            data = dataValue;
        }

        Node getNext() {
            return next;
        }

        void setNext(Node nextValue) {
            next = nextValue;
        }
    }
}