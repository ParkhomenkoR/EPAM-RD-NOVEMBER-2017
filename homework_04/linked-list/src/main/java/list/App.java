package list;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.add("1");
        myLinkedList.add("2");
        myLinkedList.add("3");
        myLinkedList.add("4");
        myLinkedList.add("5");

        System.out.println("Print: myLinkedList: \t\t" + myLinkedList);
        System.out.println(".size(): \t\t\t\t" + myLinkedList.size());
        System.out.println(".get(3): \t\t\t\t" + myLinkedList.get(3) + " (get element at index:3");
        System.out.println(".remove(2): \t\t\t\t" + myLinkedList.remove(2) + " (element removed)");
        System.out.println(".get(3): \t\t\t\t" + myLinkedList.get(3) + " (get element at index:3");
        System.out.println(".size(): \t\t\t\t" + myLinkedList.size());
        System.out.println("Print again: List: \t" + myLinkedList);
    }
}
