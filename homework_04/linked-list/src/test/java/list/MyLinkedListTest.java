package list;

import junit.framework.TestCase;

public class MyLinkedListTest extends TestCase {

    private MyLinkedList q = new MyLinkedList();
    private int size = q.size();

    public void testAdd() {
        for (int i = 0; i < size; ++i) {
            assertEquals(i, q.size());
            q.add(i);
        }
    }

    public void testRemove() {
        for (int i = 0; i < size; ++i) {
            assertEquals(size - i, q.size());
            q.remove(i);
        }
    }

    public void testSize() {
        for (int i = 0; i < size; ++i) {
            assertEquals(size - i, q.size());
            q.remove(i);
        }
        for (int i = 0; i < size; ++i) {
            assertEquals(i, q.size());
            q.add(i);
        }
    }

}